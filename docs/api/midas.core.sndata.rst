midas.core.sndata package
=========================

Submodules
----------

midas.core.sndata.meta module
-----------------------------

.. automodule:: midas.core.sndata.meta
   :members:
   :undoc-members:
   :show-inheritance:

midas.core.sndata.simulator module
----------------------------------

.. automodule:: midas.core.sndata.simulator
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: midas.core.sndata
   :members:
   :undoc-members:
   :show-inheritance:
