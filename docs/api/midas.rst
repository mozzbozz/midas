midas package
=============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   midas.adapter
   midas.cli
   midas.core
   midas.scenario
   midas.tools
   midas.util

Module contents
---------------

.. automodule:: midas
   :members:
   :undoc-members:
   :show-inheritance:
