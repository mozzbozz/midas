midas.adapter package
=====================

Module contents
---------------

.. automodule:: midas.adapter
   :members:
   :undoc-members:
   :show-inheritance:
