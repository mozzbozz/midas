# Contributors

The first ideas for midas can be traced back to Martin Tröschel and
Eric Veith. The first minimal prototype for midas was developed by
Martin Tröschel.

## Version 1

The first version of midas (0.1.0) was developed in several teams:

* Powergrid (Sanja Stark, Frauke Oest, Stephan Balduin, Martin 
  Tröschel)
* Unit Models (Johannes Gerster, Rami Elshinawy, Stephan Balduin,
  Jörg Bremer)
* Agents (Stefanie Holly, Marvin Nebel-Wenner, Sanja Stark,
  Emilie Frost, Martin Tröschel, Jörg Bremer)
* ICT (Stefanie Holly, Frauke Oest, Thomas Raub)

## Version 2

* The blackstart/mango module was contributed by Sanja Stark
* The market agents were developed by Torge Wolff
* The qmarket was developed by Thomas Wolgast

Contributors of the second version of midas (>0.1.0, by order 
of appearance):

- Stephan Balduin <stephan.balduin@offis.de>
- Sanja Stark <sanja.stark@offis.de>
- Torge Wolff <torge.wolff@offis.de>
- Thomas Wolgast <thomas.wolgast@offis.de>
- Nils Wenninghoff <nils.wenninghoff@offis.de>
