FROM python:3.9-slim

# Install OS requirements
RUN apt-get update \
    && apt-get install -y apt-utils \
    && apt-get install -y git libhdf5-dev libblas-dev liblapack-dev \
    && rm -rf /var/lib/apt/lists/*

# Create virtualenv the smart way
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN mkdir /app
WORKDIR /app

COPY . /app
RUN pip install --upgrade pip \
    && pip install wheel \
    && pip install numpy \
    && pip install numexpr \
    && pip install llvmlite \
    && pip install numba 

RUN pip install -e .
    # mkdir -p /app/midas
    # midasctl configure --autocfg && \
    # midasctl download

ENTRYPOINT ["midasctl"]
