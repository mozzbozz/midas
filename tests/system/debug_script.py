from midas.cli import cli


def main():
    cli.cli(["run", "-v", "midasmv"])


if __name__ == "__main__":
    main()
